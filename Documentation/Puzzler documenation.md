# Puzzler documenation
## 1. It’s all about the people
In your own words, describe the type of person who will be using your application once it’s finished.

- - - -

**Example:**
The end users for Puzzler are likely to be people who are new to VR, but who have already experienced various games in their life. They are probably going to be in their mid twenties and own a next-gen smartphone. And I hope they enjoy puzzle games!


- - - -
## 2. Personas
**Image:**
 ![](Puzzler%20documenation/FBF69C46-9A73-413C-9627-4FE80A414519.png)
**Name:** Anna
**Age:** 36
**Role:** Marketer
**VR Experience:** Little to none
**Quote (that sums up there attitude):** *“VR looks interesting and I’d like to give it a go.”*
**About this person:** Anna is a highly educated working mother of two. She has a busy lifestyle but is always interested in new and interesting things. She prefers to try things before jumping in head first and enjoys things she can share with her children.

- - - -
## 3. Statement of purpose
This helps us understand what we are building by defining a project scope. It should be concise and ideally no more than one or two sentences long.

**Example:**
*“Puzzler is a mobile VR app which give new VR users a quick taste of a VR via their existing smart phone.  The entire experience should take more than a few minutes.”*

- - - -


#study/vr